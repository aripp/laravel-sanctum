@extends('partials.app')
@section('content')
<div class="container my-5">
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-center justify-content-between">
                Edit user
            </div>
        </div>
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <p><b>Kesalahan : </b></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <form action="/user/{{ $user->id }}" method="post" autocomplete="off">
                @method("PUT")
                @csrf
                <div class="form-group">
                    <label for="name">Nama</label>
                    <input type="text" class="form-control form-control-sm" id="name" name="name" placeholder="nama" value="{{ old('name', $user->name) }}" required>
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control form-control-sm" id="email" name="email" placeholder="Enter email" value="{{ old('email', $user->email) }}" required>
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control form-control-sm" id="password" name="password" placeholder="Password" required>
                </div>
                <div class="form-group">
                    <label for="deskripsi">Deskripsi</label>
                    <input type="deskripsi" class="form-control form-control-sm" id="deskripsi" name="deskripsi" placeholder="Deskripsi" value="{{ old('deskripsi', $user->deskripsi) }}">
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-success">Perbarui</button>
                </div>
            </form>
        </div>
        <div class="card-footer">
            <a href="/user" class="btn btn-danger">Kembali</a>
        </div>
    </div>
</div>
@endsection

