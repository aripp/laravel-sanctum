@extends('partials.app')
@section('content')
<div class="container my-5">
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-center justify-content-between">
                Data user
                <a href="/user/create" class="btn btn-success">Tambah user</a>
            </div>
        </div>
        <div class="card-body">
            @if (session()->has('success') )
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Deskripsi</th>
                        <th>Token</th>
                        <th class="text-center">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                    <tr>
                        <td>{{$user->name}}</td>
                        <td>{{$user->deskripsi}}</td>
                        <td>
                            <ul>
                                @foreach($user->client_tokens as $token)
                                <li>{{ $token->token }}</li>
                                @endforeach
                            </ul>
                        </td>
                        <td class="text-center">
                            <a href="/user/{{ $user->id }}/token" class="btn btn-block btn-primary"> token </a>
                            <a href="/user/{{ $user->id }}/edit" class="btn btn-block btn-warning"> edit </a>
                            <a href="/user/{{ $user->id }}/delete" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')" class="btn btn-block btn-danger"> hapus </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="mt-5 text-right">
                {{ $users->links() }}
            </div>
        </div>
        <div class="card-footer">
            <a href="/logout" class="btn btn-danger">Keluar</a>
        </div>
    </div>
</div>
@endsection
