@extends('partials.app')
@section('content')
<div class="container my-5">
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-center justify-content-between">
                Data token
                <a href="/user/{{ $user->id }}/token/create" class="btn btn-success">Tambah token</a>
            </div>
        </div>
        <div class="card-body">
            @if (session()->has('success') )
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
            <blockquote class="blockquote">
                <p class="mb-0">{{$user->name}}</p>
                <p class="mb-0">{{$user->email}}</p>
                <footer class="blockquote-footer">{{ $user->deskripsi }}</footer>
            </blockquote>
            <table class="table table-sm">
                <thead>
                    <tr>
                        <th>Token</th>
                        <th class="text-center">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($user->client_tokens as $token)
                    <tr>
                        <td>{{$token->token}}</td>
                        <td class="text-center">
                            <a href="/user/{{ $user->id }}/token/{{ $token->id }}/delete" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')" class="btn btn-danger btn-sm"> hapus </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            <a href="/user" class="btn btn-danger">Kembali</a>
        </div>
    </div>
</div>
@endsection
