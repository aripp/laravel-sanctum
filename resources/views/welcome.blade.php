<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Token API Lokal</title>
    <link rel="stylesheet" href="/vendor/bootstrap/css/bootstrap.min.css">
    <style>
        .kedip {
            background: #28A745;
            color: white;
            text-decoration: none;
            padding: 8px 12px;
            border-radius: 5px;
            animation: kedip 2s linear infinite;
        }

        @keyframes kedip {
            0% {
                background: white;
                color: black;
                text-decoration: none;
            }

            50% {
                background: white;
                color: black;
                text-decoration: none;

            }

            50.1% {
                background: #28A745;
                color: white;
                text-decoration: none;

            }

            100% {
                background: #28A745;
                color: white;
                text-decoration: none;

            }
        }

    </style>
</head>
<body>
    <div class="d-flex flex-column justify-content-center align-items-center bg-danger text-white" style="height: 100vh">

        <h5 class="mb-3 text-center">"Keluarkan data lokalnya saja, aplikasi public/online taruh dihosting :)"</h5>
        <a href="/login" class="kedip">Buat token klik saya</a>

    </div>
</body>
</html>
