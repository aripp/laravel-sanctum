<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => env('USER_DEFAULT_NAME', ''),
            'email' => env('USER_DEFAULT_EMAIL', ''),
            'password' => Hash::make(env('USER_DEFAULT_PASSWORD', '')),
            'is_admin' => env('USER_DEFAULT_IS_ADMIN', false),
            'deskripsi' => env('USER_DEFAULT_DESKRIPSI', ''),
        ]);
    }
}
