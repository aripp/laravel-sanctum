<?php

namespace App\Traits;

/*
|--------------------------------------------------------------------------
| Api Responser Trait
|--------------------------------------------------------------------------
|
| This trait will be used for any response we sent to clients.
|
 */

trait ApiResponser
{
    /**
     * Return a success JSON response.
     *
     */
    protected function success($data, string $message = null, int $code = 200)
    {
        return response()->json([
            '_status' => true,
            '_message' => $message,
            '_data' => $data,
            '_time' => date("d-m-Y H:i:s"),
        ], $code);
    }

    /**
     * Return an error JSON response.
     *
     */
    protected function error(string $message = null, int $code = 200, $data = null)
    {
        return response()->json([
            '_status' => false,
            '_message' => $message,
            '_data' => $data,
            '_time' => date("d-m-Y H:i:s"),
        ], $code);
    }

}
