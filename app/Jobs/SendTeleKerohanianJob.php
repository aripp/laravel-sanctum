<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Telegram\Bot\Laravel\Facades\Telegram;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class SendTeleKerohanianJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;
    public function __construct($data)
    {
        $this->data = (object)$data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $text = "";
        switch($this->data->status){
            case "MUT_IN":
                $text = "<b>#PINDAH_BANGSAL</b>\n";
                $tag = "\n<b>#pindah_bangsal_</b>".date('Ymd');
                break;
            case "REG_IN":
                $text = "<b>#PASIEN_MASUK</b>\n";
                $tag = "\n<b>#pasien_masuk_</b>".date('Ymd');
                break;
            case "PENYESUAIAN_BED":
                $text = "<b>#PENYESUAIAN_BED</b>\n";
                $tag = "\n<b>#penyesuaian_bed_</b>".date('Ymd');

                break;
            case "REG_OUT":
                $text = "<b>#PASIEN_PULANG</b>\n";
                $tag = "\n<b>#pasien_pulang_</b>".date('Ymd');

                break;
            default:
                $text = "<b>#INFO</b>\n";
                $tag = "\n<b>#info_</b>".date('Ymd');
                
        }
        $text .= $this->data->no_rm."\n".
                    ucfirst(strtolower($this->data->nama))."\n".
                    ucfirst(strtolower($this->data->alamat))."\n".
                    ucfirst(strtolower($this->data->bangsal_nama))." (".ucfirst(strtolower($this->data->grup_bed))." / ".$this->data->no_bed.")\n".
                    $tag;

        return Telegram::sendMessage([
            'chat_id' => env("TELEGRAM_BOT_KEROHANIAN_CHANNEL"),
            'parse_mode' => 'HTML',
            'text' => $text,

        ]);
    }
}
