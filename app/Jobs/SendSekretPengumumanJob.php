<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Telegram\Bot\FileUpload\InputFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Queue\InteractsWithQueue;
use Telegram\Bot\Laravel\Facades\Telegram;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class SendSekretPengumumanJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $keterangan;
    public function __construct($keterangan)
    {
        $this->keterangan = $keterangan;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Telegram::sendDocument([
            'chat_id' => env("TELEGRAM_BOT_SEKRET_CHANNEL"), 
            'document' => InputFile::createFromContents(Storage::get('sekret/pengumuman.pdf'), 'pengumuman.pdf'),
            'caption' => $this->keterangan,
          ]);
    }
}
