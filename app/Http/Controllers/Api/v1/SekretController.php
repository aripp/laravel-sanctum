<?php

namespace App\Http\Controllers\Api\v1;

use Exception;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Jobs\SendTeleKerohanianJob;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Jobs\SendSekretPengumumanJob;
use Illuminate\Support\Facades\File;
use Telegram\Bot\Laravel\Facades\Telegram;

class SekretController extends Controller
{

    use ApiResponser;

    public function getInfo()
    {
        $response = Telegram::getUpdates();
        dd($response);
    }
    public function sendTelegram(Request $request)
    {
        $keterangan = $request->keterangan;

        if($request->hasFile('file')) {
            $uploadPath = public_path('sekret');
    
            if(!File::isDirectory($uploadPath)) {
                File::makeDirectory($uploadPath, 0755, true, true);
            }
    
            $file = $request->file('file');
    
            if($file->storeAs('sekret', 'pengumuman.pdf')) {
                SendSekretPengumumanJob::dispatch($keterangan);
            }
        }
    }

}
