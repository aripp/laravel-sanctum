<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PendaftaranController extends Controller
{

    use ApiResponser;
    /**
     * get status maintenance
     */
    public function is_maintenance()
    {
        $data = DB::connection('rsi_byl')->table('conf_daftar_online')->where('jenis', 'maintenance')->value('value');

        if (!$data) {
            return $this->error("Gagal");
        }
        return $this->success($data, 'Berhasil');
    }

    /**
     * untuk mendaftarkan pasien lama, di aplikasi pendaftaran online
     */
    public function daftar(Request $request)
    {
        $is_daftar = DB::connection('rsi_byl')->table('kunjung')
            ->where('no_rm', $request->no_rm)
            ->where('jadwal_id', $request->jadwal_id)
            ->where('tgl_masuk', $request->tgl_masuk)
            ->where('deleted', 0)
            ->count();

        if (!$is_daftar) {
            $jadwal = DB::connection('rsi_byl')->table('dokter_jadwal')
                ->where('id', $request->jadwal_id)
                ->lockForUpdate()
                ->first();
            if ($jadwal->sisa > 0) {
                $total_kunjungan = DB::connection('rsi_byl')->table('kunjung')
                    ->where('no_rm', $request->no_rm)
                    ->where('deleted', 0)
                    ->count();

                $pasien = DB::connection('rsi_byl')->table('pasien')
                    ->where('no_rm', $request->no_rm)
                    ->where('deleted', 0)
                    ->first();

                $kunjung_id = DB::connection('rsi_byl')->table('kunjung')->insertGetId([
                    'status_inden' => "YA",
                    'no_rm' => $request->no_rm,
                    'umur' => $request->umur,
                    'tgl_masuk' => $request->tgl_masuk,
                    'cara_daftar' => "Aplikasi",
                    'rujukan' => "Datang sendiri",
                    'jadwal_id' => $request->jadwal_id,
                    'waktu_praktik' => $jadwal->waktu,
                    'poli_kd' => $jadwal->poli_kd,
                    'dokter_kd' => $jadwal->dokter_kd,
                    'awal_dokter_kd' => $jadwal->dokter_kd,
                    'asuransi_id' => $request->asuransi_id,
                    'jenis_kunjung' => "Lama",
                    'kunjung_ke' => $total_kunjungan + 1,
                    'kota_kd' => $pasien->kota_kd,
                    'kecamatan_kd' => $pasien->kecamatan_kd,
                    'pj_nama' => $pasien->nama,
                    'pj_umur' => $request->umur,
                    'pj_alamat' => $pasien->alamat,
                    'pj_notelp' => $pasien->no_telp,
                    'pj_hubungan' => "DIRI SENDIRI",
                    'created' => 'inden_online',
                    'date_created' => date('Y-m-d H:i:s'),
                ]);
                return $this->success($kunjung_id, 'Berhasil');
            } else {
                return $this->error('Jadwal dokter penuh');
            }
        } else {
            return $this->error('No RM sudah terdaftar di jadwal ini');
        }
    }

    /**
     * get data pasien
     */
    public function get_pasien(Request $request)
    {
        $data = DB::connection('rsi_byl')->table('pasien')
            ->where('no_rm', $request->no_rm)
            ->where('tgl_lahir', $request->tgl_lahir)
            ->where('deleted', 0)
            ->first();

        if (!$data) {
            return $this->error("Data pasien tidak ditemukan");
        }
        return $this->success($data, 'Berhasil');
    }

    /**
     * get data jadwal dokter
     */

    public function get_jadwal(Request $request)
    {
        $data = DB::connection('rsi_byl')->table('dokter_jadwal')
            ->select([
                'dokter_jadwal.*',
                'master_dokter.dokter_nama',
                'master_poli.poli_nama'])
            ->join('master_dokter', 'dokter_jadwal.dokter_kd', '=', 'master_dokter.dokter_kd')
            ->join('master_poli', 'dokter_jadwal.poli_kd', '=', 'master_poli.poli_kd')
            ->whereNotIn('dokter_jadwal.poli_kd', ['P001', 'P006', 'P007', 'P012'])
            ->where('dokter_jadwal.hari', $request->hari)
            // ->where('dokter_jadwal.sisa', '>=', 3)
            ->where('dokter_jadwal.izin', 'Tidak')
            ->where('dokter_jadwal.deleted', 0)
            ->orderBy('dokter_jadwal.dokter_kd')
            ->get();
        if (!$data) {
            return $this->error("Tidak ada jadwal dokter");
        }
        return $this->success($data, 'Ada jadwal dokter');
    }

    /**
     * get data asuransi
     */

    public function get_asuransi(Request $request)
    {
        $data = DB::connection('rsi_byl')->table('master_asuransi')
            ->select([
                'asuransi_id',
                'asuransi_nama'])
            ->whereNotIn('asuransi_id', ['5', '6', '7', '268', '269'])
            ->where('deleted', 0)
            ->get();
        if (!$data) {
            return $this->error("tidak ada data");
        }
        return $this->success($data, 'Ada');
    }

    /**
     * get kunjangan per pasien
     */
    public function get_kunjungan(Request $request)
    {
        $data = DB::connection('rsi_byl')->table('kunjung')
            ->select([
                'kunjung.kunjung_id',
                'kunjung.status_inden',
                'kunjung.tgl_masuk',
                'kunjung.antrian',
                'master_dokter.dokter_nama'])
            ->leftJoin('master_dokter', 'kunjung.dokter_kd', '=', 'master_dokter.dokter_kd')
            ->where('kunjung.no_rm', $request->no_rm)
            ->where('kunjung.deleted', 0)
            ->orderBy('kunjung.kunjung_id', 'desc')
            ->get();

        if (!$data) {
            return $this->error("Belum ada kunjungan");
        }
        return $this->success($data, "Ada kunjungan");
    }

    /**
     * print booking
     */
    public function print_booking(Request $request)
    {
        $data = DB::connection('rsi_byl')->table('kunjung')
            ->select([
                'kunjung.kunjung_id',
                'kunjung.no_rm',
                'kunjung.umur',
                'kunjung.antrian',
                'kunjung.tgl_masuk',

                'pasien.no_ktp',
                'pasien.nama',
                'pasien.tgl_lahir',

                'master_asuransi.asuransi_nama',
                'master_dokter.dokter_nama',
                'master_poli.poli_nama',
                'dokter_jadwal.jam_mulai',
                'dokter_jadwal.jam_selesai',
                'dokter_jadwal.hari',
            ])
            ->leftJoin('pasien', 'kunjung.no_rm', '=', 'pasien.no_rm')
            ->leftJoin('dokter_jadwal', 'kunjung.jadwal_id', '=', 'dokter_jadwal.id')
            ->leftJoin('master_asuransi', 'kunjung.asuransi_id', '=', 'master_asuransi.asuransi_id')
            ->leftJoin('master_poli', 'kunjung.poli_kd', '=', 'master_poli.poli_kd')
            ->leftJoin('master_dokter', 'kunjung.dokter_kd', '=', 'master_dokter.dokter_kd')
            ->where('kunjung.kunjung_id', $request->kunjung_id)
            ->first();

        if (!$data) {
            return $this->error("Belum ada kunjungan");
        }
        return $this->success($data, "Ada kunjungan");
    }

}
