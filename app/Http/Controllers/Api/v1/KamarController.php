<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Support\Str;
use App\Traits\ApiResponser;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class KamarController extends Controller
{

    use ApiResponser;

    /**
     * get bed
     */
    public function get_bed()
    {
        $data = DB::connection('rsi_byl')->table('master_bed')
            ->select([
                'master_bed.bangsal_kd', 
                'master_bed.grup_bed', 
                'master_bed.status', 
                'master_bed_kelas.bpjs_kelas_kd', 
                'master_bed_kelas.nama as kelas', 
                'master_bangsal.bangsal_nama'])
            ->leftJoin('master_bangsal', 'master_bed.bangsal_kd', '=', 'master_bangsal.bangsal_kd')
            ->leftJoin('master_bed_kelas', 'master_bed.kelas', '=', 'master_bed_kelas.nama')
            ->where('master_bangsal.deleted', '0')
            ->where('master_bed.deleted', '0')
            ->get();

        $kamar = [];
        foreach($data as $bed){
            if($bed->kelas == "NON KELAS"){
                if($bed->bangsal_kd == 'B006'){
                    $key = $bed->bangsal_nama ." ". $bed->grup_bed . " <b>Kelas NON KELAS</b>";
                }else if($bed->bangsal_kd == 'B007'){
                    $key = $bed->grup_bed . " <b>Kelas NON KELAS</b>";
                }else{
                    $key = $bed->bangsal_nama . " <b>Kelas NON KELAS</b>";
                }
            }else{
                $key = $bed->bangsal_nama . " <b>Kelas $bed->kelas </b>" ;
            }
            if(array_key_exists($key, $kamar)){
                if($bed->status == "KOSONG"){
                    $kamar[$key]["tersedia"] += 1;
                }
                $kamar[$key]["kapasitas"] += 1;
            }else{
                $kamar[$key] = [
                    "bangsal_kd" => $bed->bangsal_kd, 
                    "kelas" => $bed->kelas, 
                    "bangsal" => $bed->bangsal_nama,
                    "kapasitas" => 1, 
                    "tersedia" =>  0,
                ];
                if($bed->status == "KOSONG"){
                    $kamar[$key]["tersedia"] += 1;
                }
            }
        }

        if (!$kamar) {
            return $this->error("Gagal");
        }
        ksort($kamar);
        return $this->success($kamar, 'Berhasil');
    }

}
