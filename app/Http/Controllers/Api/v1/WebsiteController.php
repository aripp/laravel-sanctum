<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WebsiteController extends Controller
{

    use ApiResponser;

    /**
     * get poli
     */
    public function get_poli()
    {
        $data = DB::connection('rsi_byl')->table('master_poli')
            ->whereNotIn('poli_kd', ['P006', 'P007', 'P012'])
            ->where('deleted', '0')
            ->get();

        if (!$data) {
            return $this->error("Gagal");
        }
        return $this->success($data, 'Berhasil');
    }

    /**
     * get dokter
     */
    public function get_dokter()
    {
        $data = DB::connection('rsi_byl')->table('master_dokter')
            ->whereNotIn('dokter_kd', ['2007591', '200758', 'D022', 'D023', 'D024', 'D032', 'D009'])
            ->where('dokter_nama', 'not LIKE', '%(BIDAN)%')
            ->where('dokter_nama', 'not LIKE', '%(GZ)%')
            ->where('deleted', '0')
            ->get();

        if (!$data) {
            return $this->error("Gagal");
        }
        return $this->success($data, 'Berhasil');
    }

    /**
     * get jadwal
     */
    public function get_jadwal(Request $request)
    {
        if ($request->poli_kd != "x" && $request->dokter_kd != "x") {
            $data = DB::connection('rsi_byl')->table('dokter_jadwal')
                ->select([
                    'dokter_jadwal.*',
                    'master_dokter.dokter_nama',
                    'master_poli.poli_nama'])
                ->join('master_dokter', 'dokter_jadwal.dokter_kd', '=', 'master_dokter.dokter_kd')
                ->join('master_poli', 'dokter_jadwal.poli_kd', '=', 'master_poli.poli_kd')
                ->whereNotIn('dokter_jadwal.poli_kd', ['P006', 'P007', 'P012'])
                ->where('dokter_jadwal.poli_kd', $request->poli_kd)
                ->where('dokter_jadwal.dokter_kd', $request->dokter_kd)
                ->where('dokter_jadwal.deleted', 0)
                ->orderBy('dokter_jadwal.poli_kd')
                ->get();
        } else if ($request->poli_kd != "x" && $request->dokter_kd == "x") {
            $data = DB::connection('rsi_byl')->table('dokter_jadwal')
                ->select([
                    'dokter_jadwal.*',
                    'master_dokter.dokter_nama',
                    'master_poli.poli_nama'])
                ->join('master_dokter', 'dokter_jadwal.dokter_kd', '=', 'master_dokter.dokter_kd')
                ->join('master_poli', 'dokter_jadwal.poli_kd', '=', 'master_poli.poli_kd')
                ->whereNotIn('dokter_jadwal.poli_kd', ['P006', 'P007', 'P012'])
                ->where('dokter_jadwal.poli_kd', $request->poli_kd)
                ->where('dokter_jadwal.deleted', 0)
                ->orderBy('dokter_jadwal.poli_kd')
                ->get();
        } else if ($request->poli_kd == "x" && $request->dokter_kd != "x") {
            $data = DB::connection('rsi_byl')->table('dokter_jadwal')
                ->select([
                    'dokter_jadwal.*',
                    'master_dokter.dokter_nama',
                    'master_poli.poli_nama'])
                ->join('master_dokter', 'dokter_jadwal.dokter_kd', '=', 'master_dokter.dokter_kd')
                ->join('master_poli', 'dokter_jadwal.poli_kd', '=', 'master_poli.poli_kd')
                ->whereNotIn('dokter_jadwal.poli_kd', ['P006', 'P007', 'P012'])
                ->where('dokter_jadwal.dokter_kd', $request->dokter_kd)
                ->where('dokter_jadwal.deleted', 0)
                ->orderBy('dokter_jadwal.poli_kd')
                ->get();
        } else {
            $data = DB::connection('rsi_byl')->table('dokter_jadwal')
                ->select([
                    'dokter_jadwal.*',
                    'master_dokter.dokter_nama',
                    'master_poli.poli_nama'])
                ->join('master_dokter', 'dokter_jadwal.dokter_kd', '=', 'master_dokter.dokter_kd')
                ->join('master_poli', 'dokter_jadwal.poli_kd', '=', 'master_poli.poli_kd')
                ->whereNotIn('dokter_jadwal.poli_kd', ['P006', 'P007', 'P012'])
                ->where('dokter_jadwal.deleted', 0)
                ->orderBy('dokter_jadwal.poli_kd')
                ->get();
        }
        $hari = ['SENIN', 'SELASA', 'RABU', 'KAMIS', 'JUMAT', 'SABTU', 'MINGGU'];
        $jadwal = [];

        foreach ($data as $key) {
            foreach ($hari as $h) {
                if ($key->hari == $h && $key->izin == "Tidak") {
                    $jadwal["$key->poli_nama @@ $key->dokter_nama"][$h][$key->waktu] = [
                        'dokter' => $key->dokter_nama,
                        'poli' => $key->poli_nama,
                        'hari' => $h,
                        'hariDB' => $key->hari,
                        'waktu' => $key->waktu,
                        'jadwal' => date('H:i', strtotime($key->jam_mulai)) . " - " . date('H:i', strtotime($key->jam_selesai)),
                        'kapasitas' => $key->sisa . "/" . $key->kuota,
                    ];
                } else {
                    $jadwal["$key->poli_nama @@ $key->dokter_nama"][$h][$key->waktu] = [
                        'dokter' => $key->dokter_nama,
                        'poli' => $key->poli_nama,
                        'hari' => $h,
                        'hariDB' => $key->hari,
                        'waktu' => $key->waktu,
                        'jadwal' => "x",
                        'kapasitas' => "x",
                    ];
                }
            }
        }
        foreach ($data as $key) {
            foreach ($hari as $h) {
                if ($key->hari == $h && $key->izin == "Tidak") {
                    $jadwal["$key->poli_nama @@ $key->dokter_nama"][$h][$key->waktu] = [
                        'dokter' => $key->dokter_nama,
                        'poli' => $key->poli_nama,
                        'hari' => $h,
                        'hariDB' => $key->hari,
                        'waktu' => $key->waktu,
                        'jadwal' => date('H:i', strtotime($key->jam_mulai)) . " - " . date('H:i', strtotime($key->jam_selesai)),
                        'kapasitas' => $key->sisa . "/" . $key->kuota,
                    ];
                }
            }
        }
        if (!$jadwal) {
            return $this->error("Tidak ada jadwal dokter");
        }
        return $this->success($jadwal, 'Ada jadwal dokter');

    }

    /**
     * count kamar kelas
     */
    public function count_kamar()
    {
        $VIP = DB::connection('rsi_byl')->table('master_bed')
            ->where('kelas', 'VIP')
            ->where('deleted', '0')
            ->count();
        $I = DB::connection('rsi_byl')->table('master_bed')
            ->where('kelas', 'I')
            ->where('deleted', '0')
            ->count();
        $II = DB::connection('rsi_byl')->table('master_bed')
            ->where('kelas', 'II')
            ->where('deleted', '0')
            ->count();
        $III = DB::connection('rsi_byl')->table('master_bed')
            ->where('kelas', 'III')
            ->where('deleted', '0')
            ->count();
        $NON = DB::connection('rsi_byl')->table('master_bed')
            ->where('kelas', 'NON KELAS')
            ->where('deleted', '0')
            ->count();
        $data = [
            'VIP' => $VIP,
            'I' => $I,
            'II' => $II,
            'III' => $III,
            'NON' => $NON,
        ];
        return $this->success($data, 'Berhasil');
    }

    /**
     * count get jadwal perhari
     */
    public function get_jadwal_perhari(Request $request)
    {
        $data = DB::connection('rsi_byl')->table('dokter_jadwal')
                ->select([
                    'dokter_jadwal.*',
                    'master_dokter.dokter_nama',
                    'master_poli.poli_nama'])
                ->join('master_dokter', 'dokter_jadwal.dokter_kd', '=', 'master_dokter.dokter_kd')
                ->join('master_poli', 'dokter_jadwal.poli_kd', '=', 'master_poli.poli_kd')
                ->whereNotIn('dokter_jadwal.poli_kd', ['P006', 'P007', 'P012'])
                ->where('dokter_jadwal.hari', $request->hari)
                ->where('dokter_jadwal.deleted', 0)
                ->orderBy('dokter_jadwal.poli_kd')
                ->get();
        if (!$data) {
            return $this->error("Tidak ada jadwal dokter");
        }
        return $this->success($data, 'Berhasil');
    }
}
