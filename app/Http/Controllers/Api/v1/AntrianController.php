<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class AntrianController extends Controller
{

    use ApiResponser;

    /**
     * get data antrian
     */
    public function get_antrian(Request $request)
    {
        $data = DB::connection('rsi_byl')->table('kunjung')
            ->select([
                'kunjung_id',
                'no_rm',
                'antrian',
                'antrian_status'])
            ->where('jadwal_id', $request->jadwal_id)
            ->where('tgl_masuk', $request->tanggal)
            ->where('deleted', 0)
            ->orderBy('int_antrian')
            ->get();

        if (!$data) {
            return $this->error("Gagal");
        }
        return $this->success($data, 'Berhasil');
    }

    /**
     * get data jadwal dokter
     */

    public function get_jadwal(Request $request)
    {
        $data = DB::connection('rsi_byl')->table('dokter_jadwal')
            ->select([
                'dokter_jadwal.id',
                'dokter_jadwal.jam_mulai',
                'dokter_jadwal.jam_selesai',
                'dokter_jadwal.sisa',
                'master_dokter.dokter_nama',
                'master_poli.poli_nama'])
            ->join('master_dokter', 'dokter_jadwal.dokter_kd', '=', 'master_dokter.dokter_kd')
            ->join('master_poli', 'dokter_jadwal.poli_kd', '=', 'master_poli.poli_kd')
            ->whereNotIn('dokter_jadwal.poli_kd', ['P001', 'P006', 'P007', 'P012'])
            ->where('dokter_jadwal.hari', $request->hari)
            ->where('dokter_jadwal.izin', 'Tidak')
            ->where('dokter_jadwal.deleted', 0)
            ->orderBy('dokter_jadwal.dokter_kd')
            ->get();
        if (!$data) {
            return $this->error("Tidak ada jadwal dokter");
        }
        return $this->success($data, 'Ada jadwal dokter');
    }



    /**
     * get data semua antrian per hari
     */

    public function get_antrian_all(Request $request)
    {
        $tanggal = Carbon::create($request->tanggal);
        $jadwal = DB::connection('rsi_byl')->table('dokter_jadwal')
                    ->select([
                        'dokter_jadwal.id',
                        'dokter_jadwal.jam_mulai',
                        'dokter_jadwal.jam_selesai',
                        'dokter_jadwal.hari',
                        'master_dokter.dokter_nama'
                    ])
                    ->join('master_dokter', 'dokter_jadwal.dokter_kd', '=', 'master_dokter.dokter_kd')
                    ->whereNotIn('dokter_jadwal.poli_kd', ['P001', 'P006', 'P007', 'P012', 'P015', 'P016', 'P017', 'P018', 'P021'])
                    ->where('dokter_jadwal.hari', strtoupper($tanggal->isoFormat('dddd')))
                    ->where('dokter_jadwal.deleted', 0)
                    ->get();

        $kunjungan = DB::connection('rsi_byl')->table('kunjung')
            ->select([
                'kunjung_id',
                'no_rm',
                'antrian',
                'jadwal_id',
                'tgl_masuk',
                'antrian_status',
            ])
            ->where('deleted', 0)
            ->where('tgl_masuk', $tanggal->isoFormat('YYYY-MM-DD'))
            ->get();

        $data = [];
        foreach($jadwal as $rowJadwal){
            $data[] = [
                'jadwal_id' => $rowJadwal->id,
                'dokter_nama' => $rowJadwal->dokter_nama,
                'jam_mulai' => $rowJadwal->jam_mulai,
                'jam_selesai' => $rowJadwal->jam_selesai,
                'antrean_panggil' => @$kunjungan->where('jadwal_id', $rowJadwal->id)->where('antrian_status', 'Proses')->first()->antrian ?: '-',
                'antrean_total' => $kunjungan->where('jadwal_id', $rowJadwal->id)->whereIn('antrian_status', ['Belum', 'Proses', 'Sudah', 'Lewati'])->count(),
                'antrean_sudah' => $kunjungan->where('jadwal_id', $rowJadwal->id)->whereIn('antrian_status', ['Sudah'])->count(),
            ];
        }

        if (!$data) {
            return $this->error("Tidak ada antrian");
        }
        return $this->success($data, 'Ada antrian');
    }

    /**
     * get data detail antrian
     */
    public function get_detail_antrian(Request $request)
    {
        $tanggal = Carbon::create($request->tanggal);
        $data = DB::connection('rsi_byl')->table('kunjung')
            ->select([
                'kunjung_id',
                'no_rm',
                'antrian',
                'antrian_status'])
            ->where('jadwal_id', $request->jadwal_id)
            ->where('tgl_masuk', $tanggal->isoFormat('YYYY-MM-DD'))
            ->where('deleted', 0)
            ->orderBy('int_antrian')
            ->get();

        if (!$data) {
            return $this->error("Gagal");
        }
        return $this->success($data, 'Berhasil');
    }
}
