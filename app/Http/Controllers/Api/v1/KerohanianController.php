<?php

namespace App\Http\Controllers\Api\v1;

use Exception;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Jobs\SendTeleKerohanianJob;
use App\Http\Controllers\Controller;
use Telegram\Bot\Laravel\Facades\Telegram;
use Illuminate\Support\Facades\Log;

class KerohanianController extends Controller
{

    use ApiResponser;

    public function getInfo()
    {
        $response = Telegram::getUpdates();
        dd($response);
    }
    public function sendTelegram(Request $request)
    {
        $status = $request->status;
        $inapId = $request->inap_id;
        $bedTujuan = $request->bed_id_penerima;
        if(!$status || !$inapId || !$bedTujuan){
            throw new Exception('Data tidak valid');
        }
        $data = DB::connection('rsi_byl')->table('inap_riwayat')
            ->select([
                'inap_riwayat.status', 
                'pasien.no_rm', 
                'pasien.nama', 
                'pasien.alamat', 
                'master_bed.grup_bed', 
                'master_bed.no_bed', 
                'master_bangsal.bangsal_nama'])
            ->leftJoin('master_bed', 'master_bed.bed_id', '=', 'inap_riwayat.bed_id_penerima')
            ->leftJoin('master_bangsal', 'master_bangsal.bangsal_kd' , '=', 'master_bed.bangsal_kd')
            ->leftJoin('pasien', 'pasien.no_rm', '=', 'inap_riwayat.no_rm')
            ->where('inap_riwayat.status', $status)
            ->where('inap_riwayat.inap_id', $inapId)
            ->where('inap_riwayat.bed_id_penerima', $bedTujuan)
            ->first();
            
        if($data){
            SendTeleKerohanianJob::dispatch($data);
        }
    }

}
