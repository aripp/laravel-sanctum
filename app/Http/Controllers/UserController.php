<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserToken;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    public function index()
    {
        return view("user.index", [
            'users' => User::where('is_admin', false)->latest()->paginate(100),
        ]);
    }

    public function create()
    {
        return view("user.create");
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required',
            'deskripsi' => 'required',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'deskripsi' => $request->deskripsi,
        ]);

        $token = $user->createToken($request->email)->plainTextToken;
        UserToken::create([
            'user_id' => $user->id,
            'token' => $token,
        ]);

        return redirect('/user')->with("success", "Berhasil tambah user");
    }

    public function edit(User $user)
    {
        return view("user.edit", [
            'user' => $user,
        ]);
    }

    public function update(Request $request, User $user)
    {
        $rules = [
            'name' => 'required|string|max:255',
            'password' => 'required',
            'deskripsi' => 'required',
        ];
        if ($request->email != $user->email) {
            $rules['email'] = 'required|string|email|max:255|unique:users';
        }
        $request->validate($rules);

        $user->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'deskripsi' => $request->deskripsi,
        ]);

        return redirect('/user')->with("success", "Berhasil ubah user");
    }

    public function destroy(User $user)
    {
        $user->tokens()->delete();
        UserToken::where('user_id', $user->id)->delete();
        $user->delete();

        return redirect('/user')->with("success", "Berhasil hapus user");
    }

    public function show_token(User $user)
    {
        return view("user.token", [
            'user' => $user,
        ]);
    }
    public function store_token(User $user)
    {
        $token = $user->createToken($user->email)->plainTextToken;
        UserToken::create([
            'user_id' => $user->id,
            'token' => $token,
        ]);

        return redirect('/user')->with("success", "Berhasil tambah token");
    }

    public function destroy_token(User $user, $tokenId)
    {
        $token = UserToken::find($tokenId);
        $arrSanctumId = explode("|", $token->token);
        $sanctumId = $arrSanctumId[0];

        $user->tokens()->where('id', $sanctumId)->delete();
        UserToken::destroy($tokenId);

        return redirect('/user')->with("success", "Berhasil hapus token");
    }

}
