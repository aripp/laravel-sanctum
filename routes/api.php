<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'v1', 'middleware' => ['auth:sanctum']], function () {

    // aplikasi pendaftaran online, info kamar, info antrian
    Route::post('/pendaftaran/daftar', [App\Http\Controllers\Api\v1\PendaftaranController::class, 'daftar']);
    Route::get('/pendaftaran/is_maintenance', [App\Http\Controllers\Api\v1\PendaftaranController::class, 'is_maintenance']);
    Route::post('/pendaftaran/get_pasien', [App\Http\Controllers\Api\v1\PendaftaranController::class, 'get_pasien']);
    Route::get('/pendaftaran/get_asuransi', [App\Http\Controllers\Api\v1\PendaftaranController::class, 'get_asuransi']);
    Route::post('/pendaftaran/get_jadwal', [App\Http\Controllers\Api\v1\PendaftaranController::class, 'get_jadwal']);
    Route::post('/pendaftaran/get_kunjungan', [App\Http\Controllers\Api\v1\PendaftaranController::class, 'get_kunjungan']);
    Route::post('/pendaftaran/print_booking', [App\Http\Controllers\Api\v1\PendaftaranController::class, 'print_booking']);
    Route::post('/antrian/get_antrian', [App\Http\Controllers\Api\v1\AntrianController::class, 'get_antrian']);
    Route::post('/antrian/get_jadwal', [App\Http\Controllers\Api\v1\AntrianController::class, 'get_jadwal']);
    Route::post('/antrian/get_detail_antrian', [App\Http\Controllers\Api\v1\AntrianController::class, 'get_detail_antrian']);
    Route::post('/antrian/get_antrian_all', [App\Http\Controllers\Api\v1\AntrianController::class, 'get_antrian_all']);
    Route::get('/kamar/get_bed', [App\Http\Controllers\Api\v1\KamarController::class, 'get_bed']);

    // website
    Route::get('/website/get_poli', [App\Http\Controllers\Api\v1\WebsiteController::class, 'get_poli']);
    Route::get('/website/get_dokter', [App\Http\Controllers\Api\v1\WebsiteController::class, 'get_dokter']);
    Route::post('/website/get_jadwal', [App\Http\Controllers\Api\v1\WebsiteController::class, 'get_jadwal']);
    Route::post('/website/get_jadwal_perhari', [App\Http\Controllers\Api\v1\WebsiteController::class, 'get_jadwal_perhari']);
    Route::get('/website/count_kamar', [App\Http\Controllers\Api\v1\WebsiteController::class, 'count_kamar']);

    // simrs
    Route::get('/kerohanian/bot-info', [App\Http\Controllers\Api\v1\KerohanianController::class, 'getInfo']);
    Route::post('/kerohanian/bot-send', [App\Http\Controllers\Api\v1\KerohanianController::class, 'sendTelegram']);

    // sekret
    Route::get('/sekret/bot-info', [App\Http\Controllers\Api\v1\SekretController::class, 'getInfo']);
    Route::post('/sekret/bot-send', [App\Http\Controllers\Api\v1\SekretController::class, 'sendTelegram']);
});
