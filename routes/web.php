<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Jobs\SendSekretPengumumanJob;
use Illuminate\Support\Facades\Route;

Route::middleware(['guest'])->group(function () {
    Route::view('/', 'welcome')->name('welcome');
    Route::get('/login', [AuthController::class, 'index']);
    Route::post('/authenticate', [AuthController::class, 'authenticate']);
});

Route::middleware(['auth'])->group(function () {
    Route::get('/logout', [AuthController::class, 'logout']);
    Route::get('/user', [UserController::class, 'index']);
    Route::get('/user/create', [UserController::class, 'create']);
    Route::post('/user', [UserController::class, 'store']);
    Route::get('/user/{user}/edit', [UserController::class, 'edit']);
    Route::put('/user/{user}', [UserController::class, 'update']);
    Route::get('/user/{user}/delete', [UserController::class, 'destroy']);
    Route::get('/user/{user}/token', [UserController::class, 'show_token']);
    Route::get('/user/{user}/token/create', [UserController::class, 'store_token']);
    Route::get('/user/{user}/token/{id}/delete', [UserController::class, 'destroy_token']);

});

